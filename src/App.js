import React from "react";
import ItemMenu from "./component/ItemMenu";
import './App.css'
import { v4 as uuidv4 } from 'uuid';
import FilterCartItem from "./component/FilterCartItem";
import NewCartItem from "./component/NewCartItem";
import EditCartItem from "./component/EditCartItem";

class App extends React.Component {
  state = {
    items: [
      {
        id: uuidv4(),
        name: "Burger",
        price: 50,
        category: "Food",
        image: "https://cdn-icons-png.flaticon.com/512/878/878052.png"
      },
      {
        id: uuidv4(),
        name: "Pizza",
        price: 100,
        category: "Food",
        image: "https://cdn-icons-png.flaticon.com/512/3132/3132693.png"
      },
      {
        id: uuidv4(),
        name: "Fries",
        price: 25,
        category: "Food",
        image: "https://cdn-icons-png.flaticon.com/512/1057/1057356.png"
      },
      {
        id: uuidv4(),
        name: "Coffee",
        price: 35,
        category: "Drink",
        image: "https://cdn-icons-png.flaticon.com/512/119/119248.png"
      },
      {
        id: uuidv4(),
        name: "Iced Tea",
        price: 45,
        category: "Drink",
        image: "https://cdn-icons-png.flaticon.com/512/571/571549.png"
      },
      {
        id: uuidv4(),
        name: "Hot Tea",
        price: 45,
        category: "Drink",
        description: "",
        image: "https://image.flaticon.com/icons/png/512/2764/2764121.png"
      }
    ],
    cart: [],
    total: 0,
    selectCategory: '',
    editItem: ''
  }

  orderTotal = (array) => {
    let sum = 0; 
    array.forEach(cartItem => {
      if(cartItem.quantity != null){
        cartItem.subtotal = cartItem.price * cartItem.quantity;
        sum += cartItem.subtotal;
      }
    });

    this.setState({
      total: sum
    })
    
  }

  addToMenu = (item) => {
    let itemsCopy = [...this.state.items];
    let exists = false;

    itemsCopy.map(existingItem => {
      if (item.name === existingItem.name){
        alert("the item exists")
        return exists = true;
      }
      return exists;
    });
    if (exists === false) {
      itemsCopy = [...this.state.items, item]
    }

    this.setState({
      items: itemsCopy,
    });
  }

  editItemFromMenu = (item) => {
    let itemsCopy = [...this.state.items];                                
    console.log(itemsCopy)
    itemsCopy.forEach(menuItem => {
      if (item.id === menuItem.id) {
        menuItem.name = item.name;
        menuItem.price = item.price;
        menuItem.category = item.category;
        menuItem.image = item.image;
      }
    });

    this.orderTotal(itemsCopy);
   
    this.setState({
      items: itemsCopy,
      editItem: ''
    })
  }

  addToCartItem = (item) => {
    let cartCopy = [...this.state.cart];
    let exists = false;
    
    cartCopy.forEach(cartItem => {
      if(item.id === cartItem.id){
        cartItem.quantity++;
        exists = true;
      }
    });
    
    if(exists === false){
      item.quantity = 1;
      cartCopy.push(item);
    }

    this.orderTotal(cartCopy);

    this.setState({
      cart: cartCopy,
    });
  }

  changeItemQuantity = (item) => {
    let updatedCart = this.state.cart.map(cartItem => {
      return cartItem.id === item.id ? item : cartItem;
    });

    this.orderTotal(updatedCart);

    this.setState({
      cart: updatedCart
    })
  }

  removeToCartItem = (item) => {
    let cartCopy = [...this.state.cart];

    cartCopy.forEach(cartItem => {
      if (item.id === cartItem.id) {
        cartCopy.pop(item);
      }
    });

    this.orderTotal(cartCopy);

    this.setState({
      cart: cartCopy,
    });
  }

  getCategory = (category) => {
    this.setState({
      selectCategory: category
    })
  }

  getEditItem = (item) => {
    this.setState({
      editItem: item
    })
  }

  render () {
    //convert to ternary
    let filteredItem = (this.state.selectCategory === '') ? this.state.items : this.state.items.filter(item => item.category === this.state.selectCategory);
    
    return (
      <div className="App">
        <h1>Resto App</h1>
        <FilterCartItem filteredCat={this.getCategory}/>
        {
          this.state.editItem === '' ? < NewCartItem add={this.addToMenu} /> : <EditCartItem edit={this.state.editItem} editMenu={this.editItemFromMenu} />
        }
        <br />
        <header className="App-header">
          {
            filteredItem.length === 0 ? <p>"No item available on selected category"</p> :
            filteredItem.map(item => {
              return <ItemMenu key={item.id} item={item} addToCart={this.addToCartItem} edit={this.getEditItem} type="menuItems"/>
            })
          }
        </header>
        <h2>Cart Php {this.state.total}</h2>
        <div className="App-header">
          {
            this.state.cart.map(cart => {
              return <ItemMenu item={cart} key={cart.id} addQuantity={this.changeItemQuantity} removeFromCart={this.removeToCartItem} type="cartItems" />
            })
          }
        </div>
      </div>
    );
  }
}

export default App;