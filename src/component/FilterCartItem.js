import React from "react"
import { useState } from "react";


const FilterCartItem = (props) => {
    const [category, setCategory] = useState('');

    const sentCatToParent = (category) => {
        props.filteredCat(category)
    }

    return (
        <div className="form">
            <select value={category} onChange={event => { setCategory(sentCatToParent(event.target.value)) }}>
                <option value="">All</option>
                <option value="Food">Foods</option>
                <option value="Drink">Drinks</option>
                <option value="Dessert">Desserts</option>
            </select>
        </div>
    );
}
export default FilterCartItem;