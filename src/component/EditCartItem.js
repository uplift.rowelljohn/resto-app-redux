import React from "react";
import './Form.css';
import { useState } from "react";

const EditCartItem = (props) => {
    //manual assignment of state
    const [id, setId] = useState(props.edit.id);
    const [name, setName] = useState(props.edit.name);
    const [price, setPrice] = useState(props.edit.price);
    const [category, setCategory] = useState(props.edit.category);
    const [image, setImage] = useState(props.edit.image);
    const [editFormCartItem, setEditFormCartItem] = useState(true);

    const handleSubmit = (e) => {
        e.preventDefault();
        let item = {}
        item.id = id;
        item.name = name;
        item.price = price;
        item.category = category;
        item.image = image;

        setId('');
        setName('');
        setPrice('');
        setCategory('');
        setImage('');
        setEditFormCartItem(false);

        props.editMenu(item)
        
    }
    
    const showNewCartItem = () => {
        this.setState({
            editFormCartItem: true
        })
    }

    const clearFields = () => {
        let item = {}
        setId('');
        setName('');
        setPrice('');
        setCategory('');
        setImage('');
        setEditFormCartItem(false);

        props.editMenu(item);
    }
    const showEditForm = () => {
        this.setState({
            editFormCartItem: true
        })
    }

    return(
        <div>
        {
            props.edit === null ? showEditForm : ''
            
        }
        {
            editFormCartItem ?
                '' :
                <button onClick={showNewCartItem}>Add Item to Menu</button>
        }    
        {
            editFormCartItem  ?
                <div className="form">
                    <form onSubmit={handleSubmit}>
                        <label>Item Name:
                            <input
                                type="text"
                                name="name"
                                value={name}
                                onChange={event => setName(event.target.value)}
                            />
                        </label>
                        <br />
                        <label>Item Price:
                            <input
                                type="number"
                                name="price"
                                value={price}
                                onChange={event => setPrice(event.target.value)}
                            />
                        </label>
                        <br />
                        <label>Item Category
                            <select name="category" value={category} onChange={event => setCategory(event.target.value)}>
                                <option value="Food">Foods</option>
                                <option value="Drink">Drinks</option>
                                <option value="Dessert">Desserts</option>
                            </select>
                        </label>
                        <br />
                        <label>Item Image:
                            <input
                                type="text"
                                name="image"
                                value={image}
                                onChange={event => setImage(event.target.value)}
                            />
                        </label>
                        <br />
                        <input type="submit" value="Submit" onClick={handleSubmit}/>
                        <input type="button" onClick={clearFields} value="Cancel"/>
                    </form>
                </div>
                : ''
        }
        </div>
    )
}

export default EditCartItem;