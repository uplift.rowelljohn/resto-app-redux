import {React, useState} from "react";
import './Form.css';
import { v4 as uuidv4 } from 'uuid';

const NewCartItem = (props) => {
    //manual assignment of state
    const [name, setName] = useState("");
    const [price, setPrice] = useState("");
    const [category, setCategory] = useState('Food');
    const [image, setImage] = useState("");
    const [showFormNewCartItem, setShowFormNewCartItem] = useState(false);


    const handleSubmit = (e) => {
        e.preventDefault();
        let item = {}
        item.id = uuidv4();
        item.name = name;
        item.price = price;
        item.category = category;
        item.image = image;

        setName('');
        setPrice('');
        setCategory('');
        setImage('');
        setShowFormNewCartItem(false);

        console.log(item)
        props.add(item)
        
    }
    
    const showNewCartItem = () => {
        setShowFormNewCartItem(true)
    }

    const closeNewCartItem = () => {
        setShowFormNewCartItem(false)
    }

    const clearFields = () => {
        setName('');
        setPrice('');
        setCategory('');
        setImage('');
    }
    return(
        <div>
        {
            showFormNewCartItem ?
                <button onClick={closeNewCartItem}>Close</button> :
                <button onClick={showNewCartItem}>Add Item to Menu</button>
        }    
        {
            showFormNewCartItem  ?
                <div className="form">
                    <form onSubmit={handleSubmit}>
                        <label>Item Name:
                            <input
                                type="text"
                                name="name"
                                value={name || ''}
                                onChange={event => setName(event.target.value)}
                            />
                        </label>
                        <br />
                        <label>Item Price:
                            <input
                                type="number"
                                name="price"
                                value={price || ''}
                                onChange={event => setPrice(event.target.value)}
                            />
                        </label>
                        <br />
                        <label>Item Category
                                <select name="category" value={category || ''} onChange={event => setCategory(event.target.value)}>
                                <option value="Food">Foods</option>
                                <option value="Drink">Drinks</option>
                                <option value="Dessert">Desserts</option>
                            </select>
                        </label>
                        <br />
                        <label>Item Image:
                            <input
                                type="text"
                                name="image"
                                value={image || ''}
                                onChange={event => setImage(event.target.value)}
                            />
                        </label>
                        <br />
                        <input type="submit" value="Submit" onClick={handleSubmit}/>
                        <input type="button" onClick={clearFields} value="Clear"/>
                    </form>
                </div>
                : ''
        }
        </div>
    )
}

export default NewCartItem;