import './ItemMenu.css';

const ItemMenu = (props) => {

    const addToCartBtnClkHandler = () => {
        props.addToCart(props.item)
    }
    const editToCartBtnClkHandler = () => {
        props.edit(props.item)
    }
    const deleteItemBtnClkHandler = () => {
        props.removeFromCart(props.item)
    }

    const quantityItemBtnClkHandler = (operation) => {
        if(operation === "+"){
            props.item.quantity++;
        }else{
            if (props.item.quantity >= 1){
                props.item.quantity--;
            }
        }
        props.addQuantity(props.item)
    }

    return (
        <div className="ItemMenu">
            <img src={props.item.image} alt={props.item.name} width={100}/>
            <strong>{props.item.name}</strong>
            <p><small>Php {props.item.price}</small></p>
            {
                props.type === 'menuItems' ? 
                <p>
                    <button onClick={editToCartBtnClkHandler}>Edit</button> | <button onClick={addToCartBtnClkHandler}>Order</button>
                </p> : //else display cart item details
                    <p>
                        <button onClick={() => quantityItemBtnClkHandler('-') }>-</button> 
                            {props.item.quantity} 
                        <button onClick={() => quantityItemBtnClkHandler('+') }>+</button>
                    <br /><br />
                    <span>Subtotal: Php {props.item.subtotal}</span>
                        <br />
                        <button onClick={() => deleteItemBtnClkHandler('+')}>Delete</button>
                    </p>
            }
        </div>
    );
}

export default ItemMenu;